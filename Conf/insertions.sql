Insert into users values(NULL, "a@a.com", "1", "habby", "botato");

insert into products values(NULL, "bike", "nice shiny bike", 5000, 1000);
insert into products values(NULL, "lollipop", "sticky but edible", 10000.1, 1);
insert into products values(NULL, "used underwear", "needs washing", 0.1, 300);
insert into products values(NULL, "half eaten sandwich", "tasty!", 10, 1);

insert into images values (NULL, "https://bobmoler.files.wordpress.com/2013/04/saturnmay2013.png");
insert into images values (NULL, "http://41.media.tumblr.com/7c70cb40b30893b66f08ecba9e3c0290/tumblr_mps80fR1BR1qjkedbo1_1280.jpg");

insert into user_avatar_image values(1,2);

insert into product_has_image values(1,1);

# select * from products;
# select * from users;
# select * from user_cart;
insert into user_cart values (1 , 1, 10);
insert into user_cart values (1, 2, 1);

# delete from user_cart;

# insert into table values on duplicate key update
