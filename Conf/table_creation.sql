#Database name is assumed to be eshop
#Create Database eshop;
#drop database eshop;
#use eshop;
Create table if not exists users(id integer auto_increment not null primary key,
    email varchar(50) not null unique,
    pass varchar(50) not null,
    first_name varchar(20) not null,
    last_name varchar(20) not null);

Create table if not exists products(id integer auto_increment not null primary key,
    name varchar(50) not null,
    description varchar(255) not null,
    price decimal(10,2) not null,
    stock integer default 0);

Create table if not exists user_buys_product(user_id integer,
    product_id integer,
    count integer,
    primary key (user_id, product_id),
    foreign key (user_id) references users(id) on delete cascade,
    foreign key (product_id) references products(id) on delete cascade);

Create table if not exists images(id integer auto_increment not null primary key,
    url varchar(255) not null);

Create table if not exists user_avatar_image(user_id integer,
    image_id integer,
    primary key(user_id),
    foreign key(user_id) references users(id) on delete cascade,
    foreign key(image_id) references images(id) on delete cascade);

Create table if not exists product_has_image(product_id integer,
    image_id integer,
    primary key(product_id, image_id),
    foreign key(product_id) references products(id) on delete cascade,
    foreign key(image_id) references images(id) on delete cascade);

Create table if not exists user_cart(user_id integer,
    product_id integer,
    count integer,
    primary key (user_id, product_id),
    foreign key (user_id) references users(id) on delete cascade,
    foreign key (product_id) references products(id) on delete cascade);
