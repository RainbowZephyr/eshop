<?php
include('header.php');
echo"<link rel='stylesheet' href='main.css' type='text/css'>";
session_start();
$connection = new mysqli("localhost", "root", "","eshop") or die("Failed to connect to server");

// error_reporting(0);


if(isset($_POST['signup'])){
	$email = mysqli_real_escape_string($connection, $_POST['email']);
	$password = mysqli_real_escape_string($connection, $_POST['password']);
	$repassword = mysqli_real_escape_string($connection, $_POST['repassword']);
	$fname = mysqli_real_escape_string($connection, $_POST['fname']);
	$lname = mysqli_real_escape_string($connection, $_POST['lname']);
	$image = mysqli_real_escape_string($connection, $_POST['image']);

	$_SESSION['email'] = $email;
	$_SESSION['fname'] = $fname;
	$_SESSION['lname'] = $lname;
	$_SESSION['image'] = $image;
	if($_POST['email'] && $_POST['password'] && $_POST['repassword'] && $_POST['fname'] && $_POST['lname']){

		$check = mysqli_query($connection, "SELECT * FROM `users` WHERE `email`='$email'");
		if(mysqli_num_rows($check) > 0){
			$_SESSION['userExists'] = 'Email already exists.';
			header("location: register.php");
			die();
		}
		if($password != $repassword){
			$_SESSION['wrongPass'] = 'Passwords do not match.';
			header("location: register.php");
			die();
		}
		mysqli_query($connection, "INSERT INTO `users` (`email`,`pass`,`first_name`,`last_name`) VALUES ('$email', '$password', '$fname', '$lname')");
		$user_id = mysqli_insert_id($connection);
		if ($image != ''){
		mysqli_query($connection, "INSERT INTO `images`(`url`) VALUES('$image')");
		$image_id = mysqli_insert_id($connection);
		mysqli_query($connection, "INSERT INTO `user_avatar_image` (`user_id`, `image_id`) VALUES ('$user_id', '$image_id')");
		}
		unset($_SESSION['email']);
		unset($_SESSION['fname']);
		unset($_SESSION['lname']);
		unset($_SESSION['image']);
		setcookie("current_user", $email, time()+24*60*60, "/");
		header("location: home.php");
		die();
	}
}

echo"
	<div class='formContainer'>
		<div class='form'>
			<h1> Register </h1>
			<div style='color:red;'>
			";
			if (isset($_SESSION['userExists'])){
				echo $_SESSION['userExists'];
				unset($_SESSION['userExists']);
			}
			if (isset($_SESSION['nameSymbols'])){
				echo $_SESSION['nameSymbols'];
				unset($_SESSION['nameSymbols']);
			}
			if (isset($_SESSION['wrongPass'])){
				echo $_SESSION['wrongPass'];
				unset($_SESSION['wrongPass']);
			}
			echo"
			</div>
			<form action='' method='post'>
				<table>
				<tr>
					<td>
					<p>Email:</p>
					</td>
					<td>
						<input type='text' name='email'
						";
						if (isset($_SESSION['email'])){
							echo "value ='" . $_SESSION['email'] . "'";
							unset($_SESSION['email']);
						}
						echo"
						style='padding: 4px;' required/>
					</td>
				</tr>
				<tr>
					<td>
					<p>Password:</p>
					</td>
					<td>
						<input type='password' name='password'  pattern='.{8,20}' style='padding: 4px;' required title='Password must be 8 to 20 characters long.'/>
					</td>
				</tr>
				<tr>
					<td>
					<p>Retype Password:</p>
					</td>
					<td>
						<input type='password' name='repassword' pattern='.{8,20}' style='padding: 4px;' required title='Password must be 8 to 20 characters long.'/>
					</td>
				</tr>
				<tr>
					<td>
					<p>First Name:</p>
					</td>
					<td>
						<input type='text' name='fname'
						";
						if (isset($_SESSION['fname'])){
							echo "value ='" . $_SESSION['fname'] . "'";
							unset($_SESSION['fname']);
						}
						echo"
						style='padding: 4px;' required/>
					</td>
				</tr>
				<tr>
					<td>
					<p>Last Name:</p>
					</td>
					<td>
						<input type='text' name='lname'
						";
						if (isset($_SESSION['lname'])){
							echo "value ='" . $_SESSION['lname'] . "'";
							unset($_SESSION['lname']);
						}
						echo"
						style='padding: 4px;' required/>
					</td>
				</tr>
				<tr>
					<td>
					<p>Profile Image URL:</p>
					</td>
					<td>
						<input type='text' name='image' placeholder='(Optional)'
						";
						if (isset($_SESSION['image'])){
							echo "value ='" . $_SESSION['image'] . "'";
							unset($_SESSION['image']);
						}
						echo"
						style='padding: 4px;'/>
					</td>
				</tr>
				<tr>
					<td>
					<input type='submit' value ='Signup' name ='signup'/>
					</td>
				</table>
			</form>
			Got an account? <a href='login.php'>Login</a>
		</div>
	</div>
";
include('footer.php');
?>
