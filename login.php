<?php
	include('header.php');
	echo"<link rel='stylesheet' href='main.css' type='text/css'>";
	session_start();
	$connection = new mysqli("localhost", "root", "","eshop") or die("Failed to connect to server");
	// error_reporting(0);

	if(isset($_POST['login'])){
		$email = mysqli_real_escape_string($connection, $_POST['email']);
		$password = mysqli_real_escape_string($connection, $_POST['password']);
		$user = mysqli_query($connection, "SELECT * FROM users WHERE email = '$email'");
		$row = mysqli_fetch_assoc($user);
		$_SESSION['email'] = $email;
		//$_SESSION['user_id'] = $row['id'];
		if($_POST['email'] && $_POST['password']){

			if (mysqli_num_rows($user) == 0){
				$_SESSION['userIncorrect'] = 'Email incorrect.';
				header("location: login.php");
				die();
			}
			if($row["pass"] != $password){
				$_SESSION['passwordIncorrect'] = 'Password incorrect.';
				header("location: login.php");
				die();
			}
			setcookie("current_user", $email, time()+24*60*60, "/");
			header("location: home.php");
			die();
		}
	}


	echo"
		<div class='formContainer'>
			<div class='form'>
				<h1> Login </h1>
				";
				if(isset($_GET['ref'])){
					echo"You must login or register to buy products";
				}
				echo"
				<div style='color:red;'>
				";
				if (isset($_SESSION['userIncorrect'])){
					echo $_SESSION['userIncorrect'];
					unset($_SESSION['userIncorrect']);
				}
				if (isset($_SESSION['passwordIncorrect'])){
					echo $_SESSION['passwordIncorrect'];
					unset($_SESSION['passwordIncorrect']);
				}
				echo"
				</div>
				<form action='' method='post'>
					<table>
					<tr>
						<td>
						<p>Username:</p>
						</td>
						<td>
							<input type='text' name='email' ";

							echo" style='padding: 4px;' required/>
						</td>
					</tr>
					<tr>
						<td>
						<p>Password:</p>
						</td>
						<td>
							<input type='password' name='password' style='padding: 4px;' required/>
						</td>
					</tr>
					<tr>
						<td>
						<input type='submit' value ='Login' name ='login'/>
						</td>
					</tr>
					</table>
				</form>
				No account? <a href='register.php'>Register</a>
			</div>
		</div>
	";
	include('footer.php');
?>
