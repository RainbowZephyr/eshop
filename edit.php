<?php
include('header.php');
echo"<link rel='stylesheet' href='main.css' type='text/css'>";
session_start();
$connection = new mysqli("localhost", "root", "","eshop") or die("Failed to connect to server");

// error_reporting(0);
	if(!isset($_COOKIE['current_user'])){
    header("Location: login.php?ref=cart");
	}
	$old_email = $_COOKIE['current_user'];

	$user = mysqli_query($connection, "SELECT * FROM users WHERE email = '$old_email'");
	$row_user = mysqli_fetch_assoc($user);
	$user_id = $row_user['id'];
	$image_user = mysqli_query($connection, "SELECT * FROM user_avatar_image WHERE user_id = '$user_id'");
	$row = mysqli_fetch_assoc($image_user);
	$image_url = '';
	if(!empty($row)){
		$image_id = $row['image_id'];
		$image_url = $connection->query("SELECT url FROM images WHERE id = $image_id")->fetch_object()->url;
	}

	#$image = mysqli_query($connection, "SELECT * FROM images WHERE image_id = '$image_id'");
	#$row = mysqli_fetch_assoc($image);
	#$image_url = $row['url'];

if(isset($_POST['edit'])){
	$email = mysqli_real_escape_string($connection, $_POST['email']);
	$old_password = mysqli_real_escape_string($connection, $_POST['old_password']);
	$password = mysqli_real_escape_string($connection, $_POST['password']);
	$repassword = mysqli_real_escape_string($connection, $_POST['repassword']);
	$fname = mysqli_real_escape_string($connection, $_POST['fname']);
	$lname = mysqli_real_escape_string($connection, $_POST['lname']);
	$image = mysqli_real_escape_string($connection, $_POST['image']);
	$old_image = mysqli_real_escape_string($connection, $_POST['old_image']);



	$_SESSION['email'] = $email;
	$_SESSION['fname'] = $fname;
	$_SESSION['lname'] = $lname;
	$_SESSION['image'] = $image;
	if($old_password == $row_user['pass']){
		if($_POST['email'] && $_POST['old_password'] && $_POST['fname'] && $_POST['lname']){

			if($old_email != $email){
				$check = mysqli_query($connection, "SELECT * FROM `users` WHERE `email`='$email'");
				if(mysqli_num_rows($check) > 0){
					$_SESSION['userExists'] = 'Email already exists.';
					header("location: edit.php");
					die();
				}
			}
			if($password != "" || $repassword != ""){
				if($password != $repassword){
					$_SESSION['wrongPass'] = 'Passwords do not match.';
					header("location: edit.php");
					die();
				}else{
					mysqli_query($connection, "UPDATE users SET pass = '$password' WHERE users.email = '$old_email'");
				}
			}
			mysqli_query($connection, "UPDATE users SET email = '$email', first_name = '$fname', last_name = '$lname' WHERE users.email = '$old_email'");
			if($old_image == ''){
				mysqli_query($connection, "INSERT INTO `images`(`url`) VALUES('$image')");
				$image_id = mysqli_insert_id($connection);
				mysqli_query($connection, "INSERT INTO `user_avatar_image` (`user_id`, `image_id`) VALUES ('$user_id', '$image_id')");
			}else{
				if ($image != $old_image){
				mysqli_query($connection, "UPDATE images SET url = '$image' WHERE images.id = '$image_id'");
				}
			}
			unset($_SESSION['email']);
			unset($_SESSION['fname']);
			unset($_SESSION['lname']);
			unset($_SESSION['image']);
			setcookie("current_user", $email, time()+24*60*60, "/");
			header("location: home.php");
			die();
		}
	}else{
		$_SESSION['wrongOldPass'] = 'Password not correct.';
		header("location: edit.php");
		die();
	}
}

echo"
	<div class='formContainer'>
		<div class='form'>
			<h1> Edit </h1>
			<div style='color:red;'>
			";
			if (isset($_SESSION['userExists'])){
				echo $_SESSION['userExists'];
				unset($_SESSION['userExists']);
			}
			if (isset($_SESSION['nameSymbols'])){
				echo $_SESSION['nameSymbols'];
				unset($_SESSION['nameSymbols']);
			}
			if (isset($_SESSION['wrongPass'])){
				echo $_SESSION['wrongPass'];
				unset($_SESSION['wrongPass']);
			}
			if (isset($_SESSION['wrongOldPass'])){
				echo $_SESSION['wrongOldPass'];
				unset($_SESSION['wrongOldPass']);
			}
			echo"
			</div>
			<form action='' method='post'>
				<table>
				<tr>
					<td>
					<p>Email:</p>
					</td>
					<td>
						<input type='text' name='email'
						";
						if (isset($_SESSION['email'])){
							echo "value ='" . $_SESSION['email'] . "'";
							unset($_SESSION['email']);
						}else{
							echo "value ='" . $row_user['email'] . "'";
						}
						echo"
						style='padding: 4px;'/>
					</td>
				</tr>
				<tr>
					<td>
					<p>Old Password:</p>
					</td>
					<td>
						<input type='password' name='old_password' placeholder='(Required)' style='padding: 4px;' required/>
					</td>
				</tr>
				<tr>
					<td>
					<p>New Password:</p>
					</td>
					<td>
						<input type='password' name='password' placeholder= '(Optional)' style='padding: 4px';autocomplete='off'/>
					</td>
				</tr>
				<tr>
					<td>
					<p>Retype New Password:</p>
					</td>
					<td>
						<input type='password' name='repassword' placeholder= '(Optional)' style='padding: 4px;'autocomplete='off'/>
					</td>
				</tr>
				<tr>
					<td>
					<p>First Name:</p>
					</td>
					<td>
						<input type='text' name='fname'
						";
						if (isset($_SESSION['fname'])){
							echo "value ='" . $_SESSION['fname'] . "'";
							unset($_SESSION['fname']);
						}else{
							echo "value ='" . $row_user['first_name'] . "'";
						}
						echo"
						style='padding: 4px;'/>
					</td>
				</tr>
				<tr>
					<td>
					<p>Last Name:</p>
					</td>
					<td>
						<input type='text' name='lname'
						";
						if (isset($_SESSION['lname'])){
							echo "value ='" . $_SESSION['lname'] . "'";
							unset($_SESSION['lname']);
						}else{
							echo "value ='" . $row_user['last_name'] . "'";
						}
						echo"
						style='padding: 4px;'/>
					</td>
				</tr>
				<tr>
					<td>
					<p>Profile Image URL:</p>
					</td>
					<td>
						<input type='text' name='image'
						";
						if (isset($_SESSION['image'])){
							echo "value ='" . $_SESSION['image'] . "'";
							unset($_SESSION['image']);
						}else{
							echo "value ='" . $image_url . "'";
						}
						echo"
						style='padding: 4px;'/>
					</td>
				</tr>
				<tr>
					<td>
					<input type='submit' value ='Edit' name ='edit'/>
					</td>
				</table>
			</form>
		</div>
	</div>
";
include('footer.php');
?>
